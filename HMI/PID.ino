float pidPrevCTE, pidSumCTE;
boolean pidFirstTime = true;
float factorP,factorD, factorI;
float pidRetValue ;
void SetupPID(float factorProportional, float factorDerivative, float factorIntegrative)
{
  pidFirstTime = true;
  pidSumCTE =0;
  factorP = factorProportional;
  factorD = factorDerivative;
  factorI = factorIntegrative;
  pidRetValue =0;
}

float UpdatePID(float cte)
{

    if(pidFirstTime)
    {
      pidFirstTime = false;
      pidPrevCTE = cte;
    }
    
    float proportional = cte * factorP;
   
    float d = pidPrevCTE - cte;
   
    float derivative = d * factorD;
    
    pidSumCTE += cte;
    float integrative = pidSumCTE * factorI;
    
    pidRetValue = proportional + derivative + integrative;
 

  return pidRetValue; 
} 

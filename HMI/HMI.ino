
/*
  HMI DMX Controller
  Created by Artica.cc, December 3, 2014.
  Released into the public domain.
  http://artica.cc/
*/


#include <Conceptinetics.h>
#include <SoftwareSerial.h>
#include <Metro.h>

#define START_ADDRESS 1
#define CHANNEL_VALUE 1
#define DMX_SLAVE_CHANNELS   1 
#define RXEN_PIN 0

#define TIMEOUT 3

DMX_Slave dmx_slave (DMX_SLAVE_CHANNELS,  RXEN_PIN);

const int heartBitLedPin = 13; 
const int sensorPin = A0; 
const int debugSensorPin = A1;

const int ledRedPin = 5; 
const int ledBluePin = 7; 
const int ledGreenPin = 6;  
const int sw1Pin = 12; 
const int sw2Pin = 11; 
const int txPin = 1; 
const int rxPin = 0; 

const int en = 3; 
const int rxen = 2; 

const int motorA0 = 10; //10
const int motorA1 = 9; //9
     
int heartBitLedState = HIGH;

int timeOutCounter = 0;

boolean DMXOK = false;

int sensorValue = 0;
int dmxValue = 0;
int desiredValue;
int actuatorValue = 0;

Metro MetroHeartBitUpdate = Metro (250);
Metro MetroDMXUpdate = Metro (50);
Metro MetroEncoderUpdate = Metro (5);
Metro MetroMotorUpdate = Metro (5);

// --------------------------------------------------------------------------- MOTORES
int MaxValue = 580;
int MinValue = 270;

//SoftwareSerial mySerial(17, 18);

void setup()
{
  SetupDMX();
  
  heartBitLedState = HIGH;
  
  DMXOK = false;
  
  //mySerial.begin(19200);
   
  pinMode(heartBitLedPin, OUTPUT);
  
  pinMode(ledRedPin, OUTPUT);
  pinMode(ledGreenPin, OUTPUT);
  pinMode(ledBluePin, OUTPUT);
  
  digitalWrite (ledRedPin, HIGH);
  digitalWrite (ledGreenPin, HIGH);
  digitalWrite (ledBluePin, HIGH);
 
  digitalWrite (en, LOW);
  digitalWrite (rxen, LOW);
  
  pinMode(motorA0, OUTPUT);
  pinMode(motorA1, OUTPUT);
  
  setPwmFrequency(motorA0, 1);
  setPwmFrequency(motorA1, 1);
  
  pinMode(rxPin, INPUT);
  pinMode(txPin, OUTPUT);

  pinMode(sw1Pin, INPUT_PULLUP);
  pinMode(sw2Pin, INPUT_PULLUP);
  
  SetupPID(3, 0 , 0);
  
  DiffTurn(0);
}

void loop()
{
  // Blink LED
  UpdateHeartBit();
  
  // Get DMX Order
  UpdateDMX();
  
  // Read Position and Update PID Controller
  UpdateEncoder();
  
  // Update Motor Value
  UpdateMotor();
}

void UpdateEncoder()
{
  if (MetroEncoderUpdate.check() == 1) 
  { 
    // Read Encoder Position
    sensorValue = analogRead(sensorPin); 
    // Update PID
    actuatorValue = UpdatePID(desiredValue-sensorValue); 
  }
}

void UpdateDMX()
{
  if (MetroDMXUpdate.check() == 1) 
  {
    dmxValue = dmx_slave.getChannelValue (CHANNEL_VALUE);
    //mySerial.println(dmxValue);
    desiredValue = map(dmxValue, 0, 255, MinValue, MaxValue);
  }
}

void UpdateMotor()
{
  if ((MetroMotorUpdate.check() == 1) && (DMXOK))
  {
   // if (abs(actuatorValue) < 10) actuatorValue = 0;

    DiffTurn(actuatorValue);
    
    if ((abs(actuatorValue)> 150) && (DMXOK))
    {
      if(actuatorValue > 0)
      {
        digitalWrite (ledGreenPin, HIGH);
        digitalWrite (ledBluePin, LOW);
      }
      else
      {
        digitalWrite (ledGreenPin, LOW);
        digitalWrite (ledBluePin, HIGH);
      }
    }
    else
    {
      digitalWrite (ledGreenPin, HIGH);
      digitalWrite (ledBluePin, HIGH);
    }
  }

}

void UpdateHeartBit () 
{
  if (MetroHeartBitUpdate.check() == 1) {
  
    // LED BLINK
    heartBitLedState = ! heartBitLedState;
    digitalWrite (heartBitLedPin, heartBitLedState);

    // DMX TIMEOUT
    if (DMXOK)
    {
      digitalWrite (ledRedPin, HIGH);
      timeOutCounter = TIMEOUT;
    }
    else 
    {
      if (!timeOutCounter)
      {
         timeOutCounter = TIMEOUT;
         digitalWrite (ledRedPin, LOW);
        digitalWrite (ledGreenPin, HIGH);
        digitalWrite (ledBluePin, HIGH);         
      } 
      else timeOutCounter--;
    }
    
    DMXOK = false;
        
  }
}

void setPwmFrequency(int pin, int divisor) {
  byte mode;
  if(pin == 5 || pin == 6 || pin == 9 || pin == 10) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 64: mode = 0x03; break;
      case 256: mode = 0x04; break;
      case 1024: mode = 0x05; break;
      default: return;
    }
    if(pin == 5 || pin == 6) {
      TCCR0B = TCCR0B & 0b11111000 | mode;
    } else {
      TCCR1B = TCCR1B & 0b11111000 | mode;
    }
  } else if(pin == 3 || pin == 11) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 32: mode = 0x03; break;
      case 64: mode = 0x04; break;
      case 128: mode = 0x05; break;
      case 256: mode = 0x06; break;
      case 1024: mode = 0x7; break;
      default: return;
    }
    TCCR2B = TCCR2B & 0b11111000 | mode;
  }
}





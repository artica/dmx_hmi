void DiffTurn(int speedLeft) {
 
 if (speedLeft > 0) 
 {  
  analogWrite(motorA0, speedLeft);
  digitalWrite(motorA1, LOW);
 }
 
 if (speedLeft < 0) 
 {  
  digitalWrite(motorA0, LOW);
  analogWrite(motorA1, map(speedLeft, 0, -255, 0, 255));
 }
 
 if (speedLeft == 0) 
 {  
  digitalWrite(motorA0, LOW);
  digitalWrite(motorA1, LOW); 
 }
  
}

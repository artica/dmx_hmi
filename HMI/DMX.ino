
/*
 Based on 
 http://sourceforge.net/p/dmxlibraryforar/wiki/Home/
*/



// the setup routine runs once when you press reset:
void SetupDMX() { 
  dmx_slave.onReceiveComplete ( OnFrameReceiveComplete );  
  
  // Enable DMX slave interface and start recording
  dmx_slave.enable (); 
  dmx_slave.setStartAddress (START_ADDRESS);

}  

// the loop routine runs over and over again forever:
void OnFrameReceiveComplete (unsigned short value )
{
    
    DMXOK = true;
    
}
